/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package MyApp;

public class App {
    public String Greeting() {
        System.out.println("Hello");
        return "Hello";
    }

    public static int square(int num) {
        return num * num;
    }

    public static int brokenSquare(int num) {
        return num;
    }

    public static void main(String[] args) {
        new App().Greeting();
    }
}
